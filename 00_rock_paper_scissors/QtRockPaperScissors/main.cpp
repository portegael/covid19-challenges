#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "crockpaperscissors.h"
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    CRockPaperScissors *m_rockPaperScissors = new CRockPaperScissors(&app);
    engine.rootContext()->setContextProperty("rockPaperScissors", m_rockPaperScissors);

    qmlRegisterUncreatableType<CGuiEnum>("Enums", 1, 0, "Pages", "Cannot create enum type in QML");
    qmlRegisterUncreatableType<CGuiEnum>("Enums", 1, 0, "Actions", "Cannot create enum type in QML");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
