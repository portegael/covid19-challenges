#include "crockpaperscissors.h"
#include <QDebug>

CRockPaperScissors::CRockPaperScissors(QObject *parent) : QObject(parent)
{

    qRegisterMetaType<Pages>("Pages");
    qRegisterMetaType<Actions>("Actions");

}

void CRockPaperScissors::setPlayerOneChoice(const CGuiEnum::Actions choice) {

    _playerOneChoice = choice;

    switch(_playerOneChoice) {
    case Actions::ROCK:
        setPlayerOneChoiceText("ROCK");
        break;
    case Actions::PAPER:
        setPlayerOneChoiceText("PAPER");
        break;
    case Actions::SCISSORS:
        setPlayerOneChoiceText("SCISSORS");
        break;
    case Actions::RANDOM:
        setPlayerOneChoiceText("RANDOM");
        break;
    }

    emit playerOneChoiceChanged();

}

void CRockPaperScissors::setAiChoice(const CGuiEnum::Actions choice) {
    _aiChoice = choice;

    switch(_aiChoice) {
    case Actions::ROCK:
        setAiChoiceText("ROCK");
        break;
    case Actions::PAPER:
        setAiChoiceText("PAPER");
        break;
    case Actions::SCISSORS:
        setAiChoiceText("SCISSORS");
        break;
    case Actions::RANDOM:
        setAiChoiceText("RANDOM");
        break;
    }

    emit aiChoiceChanged();
}

void CRockPaperScissors::startGame()
{
    qDebug() << "Start game";

    setCurrentIndex(Pages::ONEPLAYERPAGE);
}

void CRockPaperScissors::playerOneAction()
{
    qDebug() << "playerOneAction : " + QString::number(playerOneChoice());

    int currentChoice = playerOneChoice();

    if(currentChoice == Actions::RANDOM) {
        currentChoice = Actions::ROCK;
    }
    else {
        currentChoice++;
    }

    setPlayerOneChoice(static_cast<Actions>(currentChoice));
}

void CRockPaperScissors::play()
{
    // Reset resultText
    setResultText("");

    // Random AI Choice
    int aiRandomChoice = rand() % Actions::RANDOM;

    qDebug() << "aiRandomChoice " + QString::number(aiRandomChoice);



    Actions aiActionChoice = static_cast<Actions>(aiRandomChoice);

    setAiChoice(aiActionChoice);

    // If User chose random
    if(playerOneChoice() == Actions::RANDOM) {
        setPlayerOneChoice(static_cast<Actions>(rand() % Actions::RANDOM));
    }

    // Compute Result
    if(playerOneChoice() == aiActionChoice) {
        setResultText("It's a Draw");
    }
    else if((playerOneChoice() == Actions::ROCK && aiActionChoice == Actions::PAPER) ||
            (playerOneChoice() == Actions::PAPER && aiActionChoice == Actions::SCISSORS) ||
            (playerOneChoice() == Actions::SCISSORS && aiActionChoice == Actions::ROCK)) {
        setResultText("You Lose ! ");
    }
    else {
        setResultText("Yeaaaah You won !");
    }

}

void CRockPaperScissors::resetGame()
{
    setResultText("");
    setAiChoiceText("RANDOM");
    setAiChoice(Actions::RANDOM);

}
