#ifndef CROCKPAPERSCISSORS_H
#define CROCKPAPERSCISSORS_H

#include <QObject>


class CGuiEnum {
    Q_GADGET

public:
    enum Actions{
        ROCK = 0,
        PAPER,
        SCISSORS,
        RANDOM
    };
    Q_ENUM(Actions)

    enum Pages{
        HOMEPAGE = 0,
        ONEPLAYERPAGE
    };
    Q_ENUM(Pages)
};

class CRockPaperScissors : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(QString playerOneChoiceText READ playerOneChoiceText NOTIFY playerOneChoiceChanged)
    Q_PROPERTY(QString aiChoiceText READ aiChoiceText NOTIFY aiChoiceChanged)
    Q_PROPERTY(CGuiEnum::Actions playerOneChoice READ playerOneChoice WRITE setPlayerOneChoice NOTIFY playerOneChoiceChanged)
    Q_PROPERTY(CGuiEnum::Actions aiChoice READ aiChoice WRITE setAiChoice NOTIFY aiChoiceChanged)

    Q_PROPERTY(QString resultText READ resultText WRITE setResultText NOTIFY resultTextChanged)

public:

    explicit CRockPaperScissors(QObject *parent = nullptr);

    int currentIndex(void) const { return _currentIndex;}
    void setCurrentIndex(const int index) { _currentIndex = index; emit currentIndexChanged(); }

    QString playerOneChoiceText(void) const { return _playerOneChoiceText;}
    void setPlayerOneChoiceText(const QString text) { _playerOneChoiceText = text;  }
    QString aiChoiceText(void) const { return _aiChoiceText;}
    void setAiChoiceText(const QString text) { _aiChoiceText = text;  }

    CGuiEnum::Actions playerOneChoice(void) const { return _playerOneChoice; }
    void setPlayerOneChoice(const CGuiEnum::Actions  choice);
    CGuiEnum::Actions aiChoice(void) const { return _aiChoice; }
    void setAiChoice(const CGuiEnum::Actions  choice);

    QString resultText(void) const { return _resultText; }
    void setResultText(const QString text) { _resultText = text; emit resultTextChanged(); }

    Q_INVOKABLE void startGame(void);
    Q_INVOKABLE void playerOneAction(void);
    Q_INVOKABLE void play(void);

    Q_INVOKABLE void resetGame(void);

signals:
    void currentIndexChanged(void);
    void playerOneChoiceChanged(void);
    void aiChoiceChanged(void);
    void resultTextChanged(void);

private:
    int _currentIndex = 0;
    QString _playerOneChoiceText;
    QString _aiChoiceText;
    CGuiEnum::Actions _playerOneChoice = CGuiEnum::Actions::ROCK;
    CGuiEnum::Actions _aiChoice = CGuiEnum::Actions::RANDOM;

    QString _resultText;
};

typedef CGuiEnum::Actions Actions;
typedef CGuiEnum::Pages Pages;

#endif // CROCKPAPERSCISSORS_H
