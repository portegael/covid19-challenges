import QtQuick 2.9
import QtQuick.Window 2.2

import Enums 1.0

Window {
    id: root
    visible: true
    width: 1920
    height: 1080
    visibility: "FullScreen"

    title: qsTr("QtRockPaperScissors")

    property string colorUsed : "#0081bf"
    property int curScreenIdx: rockPaperScissors.currentIndex

    onCurScreenIdxChanged: {
        switch(curScreenIdx) {
        case Pages.HOMEPAGE :
            loader.source = "qrc:Pages/HomePage.qml"
            break;
        case Pages.ONEPLAYERPAGE :
            loader.source = "qrc:Pages/OnePlayerPage.qml"
            break;
        }
    }

    // Header
    Item {
        id: header
        anchors.left: parent.left
        anchors.top : parent.top
        width : parent.width
        height: parent.height * 0.15


        // Title
        Text {
            anchors.centerIn: parent
            horizontalAlignment: Text.AlignHCenter
            text: "Rock Paper Scissors"
            font.pixelSize: 50
            color: root.colorUsed
        }

        Item {
            width : parent.width * 0.33
            height: parent.height
            anchors.right: parent.right
            anchors.top: parent.top

            Rectangle {
                width : parent.width * 0.33
                height: parent.height * 0.5
                anchors.verticalCenter: parent.verticalCenter
                anchors.right : parent.right
                anchors.rightMargin: 30
                color: root.colorUsed

                opacity: exitMA.pressed ? 0.66 : 1

                Text {
                    anchors.centerIn: parent
                    text: "Exit"
                    color: "white"
                    font.pixelSize: 40
                    font.bold:true
                }

                MouseArea {
                    id: exitMA
                    anchors.fill: parent
                    onClicked: Qt.quit()
                }
            }
        }
    }

    // Separator
    Rectangle {
        id: separator
        anchors.top: header.bottom
        anchors.left: parent.left
        color: root.colorUsed
        width: parent.width
        height: 1
    }

    Loader {
        id: loader
        anchors.top: separator.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        source: "qrc:/Pages/HomePage.qml"
    }

}
