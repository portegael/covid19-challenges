import QtQuick 2.0
import QtQuick 2.9
import QtMultimedia 5.9

import Enums 1.0

Item {
    id: root
    anchors.fill: parent

    property bool isGameStarted : false

    Component.onCompleted: {
        rockPaperScissors.playerOneAction();
    }

    Audio {
        id: wheelSound
        source: "qrc:/Sound/wheel.mp3"
    }

    Column {
        anchors.fill : parent

        Row {

            width: parent.width
            height: parent.height * 0.5

            // Player One
            Item {
                height: parent.height * 0.8
                width: parent.width * 0.33
                anchors.verticalCenter: parent.verticalCenter

                Column {
                    anchors.fill : parent
                    spacing : 50
                    Image {

                        height: parent.height * 0.8

                        anchors.horizontalCenter: parent.horizontalCenter
                        fillMode: Image.PreserveAspectFit
                        source: {
                            switch(rockPaperScissors.playerOneChoice) {
                            case Actions.ROCK :
                                return "qrc:/images/rock.png"
                            case Actions.PAPER :
                                return "qrc:/images/paper.png"
                            case Actions.SCISSORS :
                                return "qrc:/images/scissors.png"
                            default:
                                return "qrc:images/random.png"
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if( ! root.isGameStarted)
                                    rockPaperScissors.playerOneAction();
                            }
                        }
                    }

                    Text {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter

                        text: rockPaperScissors.playerOneChoiceText
                        color: "#0081bf"
                        font.pixelSize: 40
                        font.bold:true
                    }
                }
            }

            // Start
            Item {
                height: parent.height * 0.8
                width: parent.width * 0.33
                anchors.verticalCenter: parent.verticalCenter

                Column {
                    id: gameColumn
                    anchors.fill : parent
                    spacing : 50

                    Image {

                        height: parent.height * 0.65

                        anchors.horizontalCenter: parent.horizontalCenter
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:images/logo.png"


                        RotationAnimation on rotation {
                            id: logoAnimation
                            running: false
                            loops: 5 // Animation.Infinite
                            from: 0
                            to: 360

                            onStopped: {
                                root.isGameStarted = false
                                wheelSound.stop()
                                rockPaperScissors.play();
                                resultRaw.visible = true
                            }
                        }
                    }

                    Text {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter

                        text: "PLAY"
                        color: "#0081bf"
                        font.pixelSize: 40
                        font.bold:true
                    }
                }

                MouseArea {
                    anchors.fill: gameColumn
                    onClicked: {

                        root.isGameStarted = true

                        rockPaperScissors.resetGame()
                        resultRaw.visible = false
                        wheelSound.play()
                        logoAnimation.start()
                    }
                }
            }

            // AI Player
            Item {
                height: parent.height * 0.8
                width: parent.width * 0.33
                anchors.verticalCenter: parent.verticalCenter

                Column {
                    anchors.fill : parent
                    spacing : 50
                    Image {

                        height: parent.height * 0.8

                        anchors.horizontalCenter: parent.horizontalCenter
                        fillMode: Image.PreserveAspectFit
                        source: {
                            switch(rockPaperScissors.aiChoice) {
                            case Actions.ROCK :
                                return "qrc:/images/rock.png"
                            case Actions.PAPER :
                                return "qrc:/images/paper.png"
                            case Actions.SCISSORS :
                                return "qrc:/images/scissors.png"
                            default:
                                return "qrc:images/random.png"
                            }
                        }
                    }

                    Text {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter

                        text: rockPaperScissors.aiChoiceText
                        color: "#0081bf"
                        font.pixelSize: 40
                        font.bold:true
                    }
                }
            }
        }

        // Result
        Row {
            id: resultRaw
            visible: false
            width: parent.width
            height: parent.height * 0.5

            // Player One
            Item {
                anchors.fill: parent

                Text {
                    anchors.centerIn: parent
                    color: "black"
                    font.pixelSize: 60
                    text: rockPaperScissors.resultText
                }

            }

        }
    }
}
