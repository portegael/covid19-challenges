import QtQuick 2.0

Item {
    anchors.fill : parent


    Column {

        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 50
        width: parent.width / 2

        Image {

            height: parent.height * 0.3

            anchors.horizontalCenter: parent.horizontalCenter

            fillMode: Image.PreserveAspectFit

            source: "qrc:/images/logo.png"
        }

        Rectangle {
            width: parent.width * 0.5
            height: parent.height * 0.1
            anchors.horizontalCenter: parent.horizontalCenter
            color: root.colorUsed
            radius: 50

            Text {
                anchors.centerIn: parent
                text: "One Player"
                color: "white"
                font.pixelSize: 40
                font.bold:true
            }

            MouseArea {
                id: startOnePlayer
                anchors.fill: parent
                onClicked: rockPaperScissors.startGame()
            }
        }
    }
}
